use actix_cors::Cors;
use actix_web::{
    error::{self, ErrorBadRequest},
    web, App, Error, HttpResponse, HttpServer,
};
use filters::Filters;
use lord::{get_faction_list, get_game_list};
use rand::seq::SliceRandom;
use rand::thread_rng;
use std::env;
pub mod filters;
pub mod lord;
pub mod lord_list;
use anyhow::Result;
use log::info;

use serde::{Deserialize, Serialize};
const API_PORT: u16 = 4010;

fn get_file_path() -> Result<String, Error> {
    match env::var("DATA_PATH") {
        Ok(p) => Ok(p),
        Err(err) => Err(error::ErrorNotFound(format!("Cannot get data path: {err}"))),
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct RandomValuesBody {
    values: Vec<String>,
}

async fn random(values: web::Json<RandomValuesBody>) -> Result<HttpResponse, Error> {
    let mut rng = thread_rng();
    let value = values.values.choose(&mut rng);
    Ok(HttpResponse::Ok().json(value))
}

async fn random_tw(filters: web::Query<Filters>) -> Result<HttpResponse, Error> {
    let data_path = get_file_path()?;
    let lord = lord::get_random_lord(&data_path, filters.into_inner());
    match lord {
        Ok(l) => Ok(HttpResponse::Ok().json(l)),
        Err(err) => Err(error::ErrorNotFound(format!(
            "Cannot deserialize JSON: {err}"
        ))),
    }
}

async fn tw_faction_list() -> Result<HttpResponse, Error> {
    let data_path = get_file_path()?;
    let factions = get_faction_list(&data_path);
    match factions {
        Ok(list) => Ok(HttpResponse::Ok().json(list)),
        Err(err) => Err(ErrorBadRequest(format!("Cannot get faction list: {err}"))),
    }
}

async fn tw_game_list() -> Result<HttpResponse, Error> {
    let data_path = get_file_path()?;
    let factions = get_game_list(&data_path);
    match factions {
        Ok(list) => Ok(HttpResponse::Ok().json(list)),
        Err(err) => Err(ErrorBadRequest(format!("Cannot get game list: {err}"))),
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();
    info!("Starting server on port: {API_PORT}");
    HttpServer::new(|| {
        let cors = Cors::permissive();
        App::new()
            .wrap(cors)
            .service(web::resource("/").route(web::post().to(random)))
            .service(web::resource("/totalWar").route(web::get().to(random_tw)))
            .service(web::resource("/totalWar/factions").route(web::get().to(tw_faction_list)))
            .service(web::resource("/totalWar/games").route(web::get().to(tw_game_list)))
    })
    .bind(("0.0.0.0", API_PORT))?
    .run()
    .await
}
