use anyhow::Result;
use serde::{Deserialize, Serialize};

use crate::{lord_list::LordList, Filters};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Lord {
    pub name: String,
    pub faction: String,
    pub game: i32,
}


pub fn get_faction_list(list_path: &str) -> Result<Vec<String>> {
    let lords = LordList::init(list_path)?;
    Ok(lords.faction_list())
}

pub fn get_game_list(list_path: &str) -> Result<Vec<i32>> {
    let lords = LordList::init(list_path)?;
    Ok(lords.game_list())
}

pub fn get_random_lord(list_path: &str, filters: Filters) -> Result<Lord> {
    let mut lords = LordList::init(list_path)?;
    Ok(lords.random_lord(filters)?)
}
