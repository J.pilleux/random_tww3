use serde::Deserialize;

#[derive(Deserialize)]
pub struct Filters {
    pub faction: Option<String>,
    pub game: Option<i32>,
}


