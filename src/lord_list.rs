use anyhow::{anyhow, Result};
use itertools::Itertools;
use rand::seq::SliceRandom;
use rand::thread_rng;
use std::fs;

use crate::filters::Filters;
use crate::lord::Lord;

#[derive(Clone)]
pub struct LordList {
    lords: Vec<Lord>,
}

impl LordList {
    pub fn init(list_path: &str) -> Result<LordList> {
        let content = fs::read_to_string(list_path)?;

        let deselialized: Vec<Lord> = serde_json::from_str(&content)?;
        Ok(LordList {
            lords: deselialized,
        })
    }

    pub fn random_lord(&mut self, filters: Filters) -> Result<Lord> {
        let mut rng = thread_rng();
        if let Some(faction_filter) = filters.faction {
            self.filter_faction(&faction_filter)?;
        }

        if let Some(game_filter) = filters.game {
            self.filter_game(game_filter)?;
        }
        let choice = self.lords.choose(&mut rng);
        match choice {
            Some(c) => Ok(Lord {
                name: String::from(&c.name),
                faction: String::from(&c.faction),
                game: c.game,
            }),
            None => Err(anyhow!("Cannot choose a Lord")),
        }
    }

    pub fn faction_list(&self) -> Vec<String> {
        self.lords
            .to_vec()
            .into_iter()
            .map(|l| l.faction.clone())
            .unique()
            .collect()
    }

    pub fn game_list(&self) -> Vec<i32> {
        self.lords
            .to_vec()
            .into_iter()
            .map(|l| l.game)
            .unique()
            .collect()
    }

    pub fn filter_faction(&mut self, faction: &String) -> Result<()> {
        let faction_list = self.faction_list();
        if !faction_list.contains(faction) {
            return Err(anyhow!("The faction {faction} is not valid"));
        }

        self.lords = self
            .lords
            .to_vec()
            .into_iter()
            .filter(|l| &l.faction == faction)
            .collect();
        Ok(())
    }

    pub fn filter_game(&mut self, game: i32) -> Result<()> {
        let games = self.game_list();
        if !games.contains(&game) {
            return Err(anyhow!("The game {game} is not valid"));
        }

        self.lords = self
            .lords
            .to_vec()
            .into_iter()
            .filter(|l| l.game == game)
            .collect();
        Ok(())
    }
}
