# BUILD STEP
FROM rust:1.68 as builder

ENV APP=random_tww3
WORKDIR /app
COPY . .
RUN cargo build --release

# RUN STEP
FROM debian:bullseye-slim
RUN apt-get update 
RUN apt-get install -y ca-certificates tzdata
RUN rm -fr /var/lib/apt/lists/*

WORKDIR /app
COPY --from=builder /app/target/release/random_tww3 /app/random_tww3
COPY ./resources/data.json /app/data.json
ENV DATA_PATH=/app/data.json
EXPOSE 4010
CMD ["/app/random_tww3"]
