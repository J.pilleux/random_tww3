DATA_PATH=./resources/data.json

all: run

build:
	docker build -t random_tww3 .

run:
	docker run -p 4010:4010 random_tww3

debug:
	DATA_PATH=$(DATA_PATH) cargo run
